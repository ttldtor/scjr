name := "scjr"

version := "0.2.0-SNAPSHOT"

assemblyMergeStrategy in assembly := {
  case PathList("module-info.class") => MergeStrategy.discard
  case x => (assemblyMergeStrategy in assembly).value(x)
}

scalaVersion := "2.12.4"

libraryDependencies += "org.scalafx" % "scalafx_2.12" % "8.0.144-R12"

libraryDependencies += "org.jsoup" % "jsoup" % "1.11.2"

libraryDependencies += "com.h2database" % "h2" % "1.4.196"

libraryDependencies += "org.flywaydb" % "flyway-core" % "5.0.7"

libraryDependencies += "org.slf4j" % "slf4j-api" % "1.8.0-beta1"

libraryDependencies += "org.slf4j" % "slf4j-simple" % "1.8.0-beta1"

libraryDependencies += "com.osinka.i18n" % "scala-i18n_2.12" % "1.0.2"

libraryDependencies += "org.scalatest" % "scalatest_2.12" % "3.2.0-SNAP10" % "test"

libraryDependencies += "org.scalacheck" %% "scalacheck" % "1.13.4" % "test"
