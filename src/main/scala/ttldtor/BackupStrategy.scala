package ttldtor

/**
  * @author ttldtor
  * @since 23.11.2017
  */
object BackupStrategy extends Enumeration {

  val MIGRATION_BACKUP = BackupStrategy("MIGRATION_BACKUP", "migration_backup.db.zip")
  val SESSION_BACKUP = BackupStrategy("SESSION_BACKUP", "session_backup.db.zip")

  case class BackupStrategy(name: String, path: String) extends Val(name)
}