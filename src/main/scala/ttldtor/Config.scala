package ttldtor

import java.util.{Locale, Properties}

import scala.util.Try

/**
  * @author ttldtor
  * @since 22.11.2017
  */
object Config {
  lazy val databaseUrl: String = prop.getProperty("db")
  lazy val user: String = prop.getProperty("user")
  lazy val password: String = prop.getProperty("password")
  lazy val locale: Locale = {
    val l = prop.getProperty("locale")

    if (l == null || l.isEmpty) {
      Locale.getDefault
    } else {
      val splitLocale = l.split("-")

      if (splitLocale.size == 1) {
        new Locale(splitLocale(0))
      } else {
        new Locale(splitLocale(0), splitLocale(1))
      }
    }
  }
  lazy val processingDelay: Int = {
    val d = prop.getProperty("processingDelay", "10")

    Try(Integer.parseInt(d)).getOrElse(10)
  }
  lazy val connectionTimeout: Int = Try(Integer.parseInt(prop.getProperty("connectionTimeout", "10000")))
    .getOrElse(10000)

  val configFileName = "app.properties"
  val prop = new Properties()

  prop.load(getClass.getClassLoader.getResourceAsStream(configFileName))
}
