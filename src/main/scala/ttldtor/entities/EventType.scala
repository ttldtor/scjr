package ttldtor.entities

/**
  * @author ttldtor
  * @since 24.11.2017
  */
object EventType extends Enumeration {
  val MESSAGE, THIRD_PERSON_MESSAGE, ENTER, EXIT = Value
}
