package ttldtor.entities

import java.util.Date

/**
  * @author ttldtor
  * @since 13.02.2018
  */
case class ParsingTask(id: Long, logSite: LogSite, fromDate: Date, toDate: Date)

object ParsingTask {
  def apply(logSite: LogSite, fromDate: Date, toDate: Date): ParsingTask =
    new ParsingTask(-1, logSite, fromDate ,toDate)
}
