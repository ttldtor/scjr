package ttldtor.entities

import java.util.Date

/**
  * @author ttldtor
  * @since 23.11.2017
  */
case class LogSite(id: Long, name: String, conference: String, url: String, lastParsedDate: Date, deleted: Boolean = false)

object LogSite {
  def apply(name: String, conference: String, url: String, lastParsedDate: Date): LogSite
  = new LogSite(-1, name, conference, url, lastParsedDate, false)
}
