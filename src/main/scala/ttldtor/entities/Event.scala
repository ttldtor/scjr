package ttldtor.entities

/**
  * @author ttldtor
  * @since 29.01.2018
  */
trait Event {
  def `type`: EventType.Value

  def timestamp: Long

  def who: String
}

trait MessageEvent extends Event {
  def message: String
}

case class EnterEvent(override val timestamp: Long, override val who: String) extends Event {
  override def `type`: EventType.Value = EventType.ENTER
}

case class ExitEvent(override val timestamp: Long, override val who: String) extends Event {
  override def `type`: EventType.Value = EventType.EXIT
}

case class SimpleMessageEvent(override val timestamp: Long, override val who: String, override val message: String)
  extends MessageEvent {
  override def `type`: EventType.Value = EventType.MESSAGE
}

case class ThirdPersonMessageEvent(override val timestamp: Long, override val who: String, override val message: String)
  extends MessageEvent {
  override def `type`: EventType.Value = EventType.THIRD_PERSON_MESSAGE
}
