package ttldtor.parsers

import java.util._

import org.jsoup.Jsoup
import org.jsoup.nodes.{Element, Node, TextNode}
import ttldtor.entities._

import scala.collection.mutable
import scala.util.matching.Regex

/**
  * @author ttldtor
  * @since 30.01.2018
  */
class CjrChatLogParser extends ChatLogParser {
  val timeStampClassName = "ts"
  val joinEventClassName = "mj"
  val leaveEventClassName = "ml"
  val messageEventClassName = "mn"
  val thirdPersonMessageClassName = "mne"
  val joinOrLeaveMessageRegex: Regex = """(.+) (?:зашёл|вышел|вышел|вошёл).+?""".r
  val messageRegex: Regex = """<(.+)> .*?""".r

  def getTimeStampElement(element: Element): Option[Element] = {
    //TODO: optimization (previousElementSibling is non optimal)
    var prev = element.previousElementSibling()

    while (prev != null) {
      if (prev.hasClass(timeStampClassName)) {
        return Some(prev)
      }

      if (prev.hasClass(joinEventClassName) || prev.hasClass(leaveEventClassName)
        || prev.hasClass(messageEventClassName) || prev.hasClass(thirdPersonMessageClassName)) {
        return None
      }

      prev = prev.previousElementSibling()
    }

    None
  }

  def collectText(element: Element): String = {
    import scala.collection.JavaConverters._

    var text: String = element.textNodes().asScala.foldLeft("")(_ + _.text())

    if (element.hasClass(messageEventClassName)) {
      var next: Node = element
      var stop = false

      next = next.nextSibling()

      while (!stop && next != null) {
        next match {
          case textNode: TextNode =>
            text += textNode.text()
          case element: Element =>
            element.tagName() match {
              case "a" =>
                if (element.hasClass(timeStampClassName)) {
                  stop = true
                } else {
                  text += element.text()
                }
              case "br" =>
                text += "\n"
            }
          case _ =>
        }

        next = next.nextSibling()
      }
    }

    text.trim
  }

  def getTimeStampMillis(element: Element): Option[Long] = {
    val timeElements = element.attr("name").split(':')

    if (timeElements.length != 3) {
      return None
    }

    val secondParts = timeElements(2).split('.')

    try {
      val hours = timeElements(0).toLong
      val minutes = timeElements(1).toLong
      val seconds = secondParts(0).toLong
      val millis = if (secondParts.length == 2) secondParts(1).toLong else 0

      Some(hours * 3600000 + minutes * 60000 + seconds * 1000 + millis)
    } catch {
      case _: NumberFormatException => None
    }
  }

  def getWho(element: Element): String = {
    val text = collectText(element)
    val result: String = Option(joinOrLeaveMessageRegex.findAllIn(text))
      .map(matchIter => if (matchIter.hasNext) matchIter.group(1) else "").getOrElse("")

    if (result.isEmpty) {
      Option(messageRegex.findAllIn(text))
        .map(matchIter => if (matchIter.hasNext) matchIter.group(1) else "").getOrElse("")
    } else {
      result
    }
  }

  def calculateMillisFrom(millis: Long, element: Element): Option[Long] =
    getTimeStampElement(element).flatMap(getTimeStampMillis).map(millis + _)

  def parseEnterEvent(millis: Long, element: Element): Option[(Long, EnterEvent)] =
    calculateMillisFrom(millis, element).map(newMillis => (newMillis, EnterEvent(newMillis, getWho(element))))

  def parseExitEvent(millis: Long, element: Element): Option[(Long, ExitEvent)] =
    calculateMillisFrom(millis, element).map(newMillis => (newMillis, ExitEvent(newMillis, getWho(element))))

  def parseMessageEvent(millis: Long, element: Element): Option[(Long, SimpleMessageEvent)] = {
    calculateMillisFrom(millis, element).map(newMillis => {
      val who = getWho(element)

      (newMillis, SimpleMessageEvent(newMillis, who, collectText(element).replaceFirst(s"<$who> ", "")))
    })
  }

  def parseThirdPersonMessageEvent(millis: Long, element: Element): Option[(Long, ThirdPersonMessageEvent)] =
    calculateMillisFrom(millis, element)
      .map(newMillis => (newMillis, ThirdPersonMessageEvent(newMillis, getWho(element), collectText(element))))

  override def parse(date: Date, fileContents: String): ParseResult = {
    import scala.collection.JavaConverters._

    val doc = Jsoup.parse(fileContents)
    val millis = date.getTime
    val joinEventElements = doc.getElementsByClass(joinEventClassName)
    val leaveEventElements = doc.getElementsByClass(leaveEventClassName)
    val messageEventElements = doc.getElementsByClass(messageEventClassName)
    val thirdPersonMessageEventElements = doc.getElementsByClass(thirdPersonMessageClassName)
    val allEvents = mutable.Map[Long, Event]()
    val enterEventList = mutable.MutableList[EnterEvent]()
    val exitEventList = mutable.MutableList[ExitEvent]()
    val messageEventList = mutable.MutableList[SimpleMessageEvent]()
    val thirdPersonMessageEventList = mutable.MutableList[ThirdPersonMessageEvent]()

    joinEventElements.asScala.foreach(element => {
      parseEnterEvent(millis, element).foreach(pair => {
        allEvents += pair
        enterEventList += pair._2
      })
    })

    leaveEventElements.asScala.foreach(element => {
      parseExitEvent(millis, element).foreach(pair => {
        allEvents += pair
        exitEventList += pair._2
      })
    })

    messageEventElements.asScala.foreach(element => {
      parseMessageEvent(millis, element).foreach(pair => {
        allEvents += pair
        messageEventList += pair._2
      })
    })

    thirdPersonMessageEventElements.asScala.foreach(element => {
      parseThirdPersonMessageEvent(millis, element).foreach(pair => {
        allEvents += pair
        thirdPersonMessageEventList += pair._2
      })
    })

    val nicknames = allEvents.flatMap(pair => {
      val who = pair._2.who

      if (who == null || who.isEmpty) {
        None
      }

      Some(who)
    }).toSet[String]

    val nickNamesList = nicknames.toList.sortBy(-_.length)

    val newThirdPersonMessageEventList = thirdPersonMessageEventList.map(event => {
      var found = false
      var newWho = event.who

      for (nick <- nickNamesList if !found) {
        if (event.message.startsWith(nick)) {
          newWho = nick
          found = true
        }
      }

      if (newWho.isEmpty) {
        newWho = event.message.split(' ')(0)
      }

      ThirdPersonMessageEvent(event.timestamp, newWho, event.message.replaceFirst(s"$newWho ", ""))
    }).toList

    ParseResult(enterEventList.toList, exitEventList.toList, messageEventList.toList, newThirdPersonMessageEventList,
      allEvents.toMap)
  }
}
