package ttldtor.parsers

import java.util.Date

/**
  * @author ttldtor
  * @since 30.01.2018
  */
trait ChatLogParser {
  def parse(date: Date, fileContents: String): ParseResult
}
