package ttldtor.parsers

import ttldtor.entities._

/**
  * @author ttldtor
  * @since 30.01.2018
  */
case class ParseResult(enterEvents: List[EnterEvent],
                       exitEvents: List[ExitEvent],
                       messageEvents: List[SimpleMessageEvent],
                       thirdPersonMessageEvents:
                       List[ThirdPersonMessageEvent],
                       allEvents: Map[Long, Event]);
