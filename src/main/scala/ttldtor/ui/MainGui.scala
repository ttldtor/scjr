package ttldtor.ui

import javafx.application.Application
import javafx.collections.FXCollections
import javafx.stage.Stage

import com.osinka.i18n.{Lang, Messages => i18n}
import ttldtor.Config
import ttldtor.dao.{LogSiteDao, ParsingTaskDao}
import ttldtor.entities.{LogSite, ParsingTask}
import ttldtor.providers.ChatLogPeriodProvider
import ttldtor.ui.logsite.{LogSiteDialog, LogSiteModel, LogSiteTable}
import ttldtor.ui.parsingtask.{ParsingTaskModel, ParsingTaskTable}

import scalafx.application.Platform
import scalafx.beans.property.ObjectProperty
import scalafx.scene.Scene
import scalafx.scene.control.Alert.AlertType
import scalafx.scene.control.{Alert, ButtonType}
import scalafx.scene.layout.VBox

/**
  * @author ttldtor
  * @since 02.02.2018
  */
class MainGui extends Application {
  implicit val userLang: Lang = Lang(Config.locale.getLanguage)

  override def start(stage: Stage): Unit = {
    import TaskUtils._

    import scala.collection.JavaConverters._

    val pane = new VBox()
    val menuBar = new CjrMenuBar()
    val logSiteTable = new LogSiteTable() {
      visible = false
      managed = false
    }
    val parsingTaskTable = new ParsingTaskTable() {
      visible = false
      managed = false
    }

    val logSites = FXCollections.observableArrayList[LogSiteModel]()
    val parsingTasks = FXCollections.observableArrayList[ParsingTaskModel]()
    val mainDialogState: ObjectProperty[MainDialogState.Value] = ObjectProperty(this, "mainDialogState",
      MainDialogState.NONE)

    mainDialogState.onChange((_, oldValue, newValue) => {
      (oldValue, newValue) match {
        case (_, MainDialogState.LOG_SITES) =>
          parsingTaskTable.visible = false
          logSiteTable.visible = true
          stage.setTitle("CJR:Log Sites")

        case (_, MainDialogState.PARSING_TASKS) =>
          logSiteTable.visible = false
          parsingTaskTable.visible = true
          stage.setTitle("CJR:Parsing Tasks")

        case _ =>
          stage.setTitle("CJR")
      }
    })

    logSiteTable.managed.bind(logSiteTable.visible)
    parsingTaskTable.managed.bind(parsingTaskTable.visible)

    logSiteTable.visibleProperty().addListener((_, _, newValue) => {
      menuBar.addLogSiteMenuItem.disable = !newValue
      menuBar.editLogSiteMenuItem.disable = !newValue
      menuBar.deleteLogSiteMenuItem.disable = !newValue
    })

    logSiteTable.setItems(logSites)
    parsingTaskTable.setItems(parsingTasks)
    pane.children.addAll(menuBar, logSiteTable, parsingTaskTable)

    runAsync {
      (
        LogSiteDao.get().map(site => new LogSiteModel(site)).asJavaCollection,
        ParsingTaskDao.get().map(parsingTask => new ParsingTaskModel(parsingTask)).asJavaCollection
      )
    } ui { models =>
      logSites.addAll(models._1)
      parsingTasks.addAll(models._2)
      mainDialogState.value = MainDialogState.LOG_SITES
    }

    menuBar.exitMenuItem.onAction = { _ => Platform.exit() }
    menuBar.viewLogSites.onAction = { _ => mainDialogState.value = MainDialogState.LOG_SITES }
    menuBar.viewParsingTasks.onAction = { _ => mainDialogState.value = MainDialogState.PARSING_TASKS }
    menuBar.addLogSiteMenuItem.onAction = { _ =>
      new LogSiteDialog(DialogType.NEW, None).showAndWait() match {
        case Some(Some(logSite: LogSite)) =>
          runAsync {
            LogSiteDao.create(logSite)
          } ui {
            for (s <- _) logSites.add(new LogSiteModel(s))
          }
        case _ =>
      }
    }

    val editSelected = () => {
      for (selected <- Option(logSiteTable.selectionModel().getSelectedItem)) {
        runAsync {
          LogSiteDao.getById(selected.id())
        } ui {
          case Some(logSite) =>
            new LogSiteDialog(DialogType.EDIT, Some(logSite)).showAndWait() match {
              case Some(Some(editedLogSite: LogSite)) =>
                runAsync {
                  LogSiteDao.save(editedLogSite)
                } ui {
                  if (_) selected.set(editedLogSite)
                }
              case _ =>
            }
          case None => logSites.remove(selected)
        }
      }
    }

    logSiteTable.onMouseClicked = { e =>
      if (e.getClickCount > 1) {
        editSelected()
      }
    }

    menuBar.editLogSiteMenuItem.onAction = { _ => editSelected() }
    menuBar.deleteLogSiteMenuItem.onAction = { _ =>
      for (selected <- Option(logSiteTable.selectionModel().getSelectedItem)) {
        runAsync {
          LogSiteDao.getById(selected.id())
        } ui {
          case Some(logSite) =>
            val alert = new Alert(AlertType.Confirmation) {
              title = i18n("alert.deleteEntity.title")
              headerText = i18n("alert.deleteLogSiteRecord.header")
            }

            alert.showAndWait() match {
              case Some(ButtonType.OK) =>
                runAsync {
                  LogSiteDao.delete(logSite)
                  parsingTasks.removeIf(_.logSite().id == logSite.id)
                } ui {
                  if (_) logSites.remove(selected)
                }
              case _ =>
            }
          case None =>
            logSites.remove(selected)
        }
      }
    }

    menuBar.parseLogsMenuItem.onAction = { _ =>
      val selected = logSiteTable.selectionModel().getSelectedItems

      if (!selected.isEmpty) {
        val model = selected.get(0)

        runAsync(ChatLogPeriodProvider(model.url())) ui {
          case Some((from, to)) =>
            runAsync(ParsingTaskDao.create(ParsingTask(-1, model.repr(), from, to))) ui {
              for (t <- _) parsingTasks.add(new ParsingTaskModel(t))
            }

          case None =>
        }
      }
    }

    stage.setTitle("CJR")
    stage.setScene(new Scene(pane, 800, 600))
    stage.show()
  }
}
