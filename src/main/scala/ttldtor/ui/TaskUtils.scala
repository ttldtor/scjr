package ttldtor.ui

import scalafx.application.Platform
import scalafx.concurrent.Task

/**
  * @author ttldtor
  * @since 30.01.2018
  */
object TaskUtils {

  implicit class UiTaskOps[T](repr: Task[T]) {
    def ui(op: (T) => Unit): Unit = success(op)

    def success(op: (T) => Unit): Task[T] = {
      Platform.runLater {
        repr.setOnSucceeded(_ => op(repr.getValue))
      }

      repr
    }
  }

  def runAsync[T](op: => T): Task[T] = task(op)

  def task[T](op: => T): Task[T] = {
    val t = Task(op)

    t.setOnFailed(_ => {
      Thread.getDefaultUncaughtExceptionHandler.uncaughtException(Thread.currentThread(), t.getException)
    })

    new Thread(t).start()
    t
  }
}