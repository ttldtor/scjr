package ttldtor.ui.logsite

import java.util.Date
import javafx.beans.value.ChangeListener

import com.osinka.i18n.{Lang, Messages => i18n}
import ttldtor.Config
import ttldtor.entities.LogSite
import ttldtor.ui.DialogType

import scalafx.scene.control.{ButtonType, Dialog, Label, TextField}
import scalafx.scene.layout.GridPane

/**
  * @author ttldtor
  * @since 30.01.2018
  */
class LogSiteDialog(dialogType: DialogType.Value, initValue: Option[LogSite]) extends Dialog[Option[LogSite]] {
  implicit val userLang: Lang = Lang(Config.locale.getLanguage)

  val grid = new GridPane()
  val nameLabel = new Label(i18n("logSiteDialog.nameLabel.text"))
  val nameText = new TextField()
  val conferenceLabel = new Label(i18n("logSiteDialog.conferenceLabel.text"))
  val conferenceText = new TextField()
  val urlLabel = new Label(i18n("logSiteDialog.urlLabel.text"))
  val urlText = new TextField()

  var readOnly = false

  dialogType match {
    case DialogType.NEW =>
      title = i18n("logSiteDialog.new.title")
      headerText = i18n("logSiteDialog.new.headerText")
    case DialogType.EDIT =>
      title = i18n("logSiteDialog.edit.title")
      headerText = i18n("logSiteDialog.edit.headerText")
    case _ =>
      title = i18n("logSiteDialog.default.title")
      headerText = i18n("logSiteDialog.default.headerText")
      readOnly = true
  }

  nameText.text = initValue.map(_.name).getOrElse("")
  conferenceText.text = initValue.map(_.conference).getOrElse("")
  urlText.text = initValue.map(_.url).getOrElse("")

  nameText.editable = !readOnly
  conferenceText.editable = !readOnly
  urlText.editable = !readOnly

  grid.addRow(0, nameLabel, nameText)
  grid.addRow(1, conferenceLabel, conferenceText)
  grid.addRow(2, urlLabel, urlText)

  resultConverter = bt => {
    if (readOnly) {
      initValue
    } else if (bt == ButtonType.OK) {
      Some(LogSite(initValue.map(_.id).getOrElse(-1), nameText.text(), conferenceText.text(), urlText.text(),
        initValue.map(_.lastParsedDate).getOrElse(new Date(0))))
    } else {
      initValue
    }
  }

  if (!readOnly) {
    val onTextChange: ChangeListener[String] = (_, _, _) => {
      // TODO: validation
      dialogPane().lookupButton(ButtonType.OK).setDisable(nameText.text().isEmpty
        && conferenceText.text().isEmpty && urlText.text().isEmpty)
    }

    nameText.text.addListener(onTextChange)
    conferenceText.text.addListener(onTextChange)
    urlText.text.addListener(onTextChange)
  }

  dialogPane().getButtonTypes.addAll(ButtonType.OK)
  dialogPane().lookupButton(ButtonType.OK).setDisable(!readOnly)
  dialogPane().setContent(grid)
}
