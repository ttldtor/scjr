package ttldtor.ui.logsite

import java.text.SimpleDateFormat

import com.osinka.i18n.{Lang, Messages => i18n}
import ttldtor.Config

import scalafx.beans.property.StringProperty
import scalafx.scene.control.{TableColumn, TableView}
import scalafx.scene.layout.Priority

/**
  * @author ttldtor
  * @since 01.02.2018
  */
object LogSiteTable {
  val DateFormat = new SimpleDateFormat("dd.MM.yyyy")
}

class LogSiteTable extends TableView[LogSiteModel] {
  implicit val userLang: Lang = Lang(Config.locale.getLanguage)

  private val idColumn = new TableColumn[LogSiteModel, Long](i18n("logSiteTable.idColumn.headerText")) {
    visible = false
    cellValueFactory = {
      _.value.id
    }
  }

  private val nameColumn = new TableColumn[LogSiteModel, String](i18n("logSiteTable.nameColumn.headerText")) {
    cellValueFactory = {
      _.value.name
    }
  }

  private val conferenceColumn = new TableColumn[LogSiteModel, String](i18n("logSiteTable.conferenceColumn.headerText")) {
    cellValueFactory = {
      _.value.conference
    }
  }

  private val urlColumn = new TableColumn[LogSiteModel, String](i18n("logSiteTable.urlColumn.headerText")) {
    cellValueFactory = {
      _.value.url
    }
  }

  private val lastUpdateColumn = new TableColumn[LogSiteModel, String](i18n("logSiteTable.lastUpdateColumn.headerText")) {
    cellValueFactory = { f =>
      val date = f.value.lastParsedDate()
      val dateString = if (date.getTime == 0) i18n("logSiteTable.lastUpdateColumn.cell.defaultText")
      else LogSiteTable.DateFormat.format(date)

      StringProperty(dateString)
    }
  }

  columns ++= List(idColumn, nameColumn, conferenceColumn, urlColumn, lastUpdateColumn)
  vgrow = Priority.Always
}
