package ttldtor.ui.logsite

import java.util.Date

import ttldtor.entities.LogSite

import scalafx.beans.property.{ObjectProperty, StringProperty}

/**
  * @author ttldtor
  * @since 01.02.2018
  */
class LogSiteModel(logSite: LogSite) {
  val repr: ObjectProperty[LogSite] = ObjectProperty(this, "repr", logSite)
  val id: ObjectProperty[Long] = ObjectProperty(this, "id", logSite.id)
  val name = new StringProperty(this, "name", logSite.name)
  val conference = new StringProperty(this, "conference", logSite.conference)
  val url = new StringProperty(this, "url", logSite.url)
  val lastParsedDate: ObjectProperty[Date] = ObjectProperty(this, "date", logSite.lastParsedDate)

  def set(logSite: LogSite): Unit = {
    repr.value = logSite
    id.value = logSite.id
    name.value = logSite.name
    conference.value = logSite.conference
    url.value = logSite.url
    lastParsedDate.value = logSite.lastParsedDate
  }
}
