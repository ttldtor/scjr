package ttldtor.ui

/**
  * @author ttldtor
  * @since 30.01.2018
  */
object DialogType extends Enumeration {
  val NEW, EDIT, VIEW, CLONE, DELETE = Value
}
