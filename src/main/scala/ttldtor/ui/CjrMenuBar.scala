package ttldtor.ui

import com.osinka.i18n.{Lang, Messages => i18n}
import ttldtor.Config

import scalafx.scene.control.{Menu, MenuBar, MenuItem, SeparatorMenuItem}

/**
  * @author ttldtor
  * @since 01.02.2018
  */
class CjrMenuBar extends MenuBar {
  implicit val userLang: Lang = Lang(Config.locale.getLanguage)

  val fileMenu = new Menu(i18n("fileMenu.label"))
  val exitMenuItem = new MenuItem(i18n("fileMenu.exitMenuItem.label"))

  val editMenu = new Menu(i18n("editMenu.label"))
  val addLogSiteMenuItem = new MenuItem(i18n("editMenu.addLogSiteMenuItem.label"))
  val editLogSiteMenuItem = new MenuItem(i18n("editMenu.editLogSiteMenuItem.label"))
  val deleteLogSiteMenuItem = new MenuItem(i18n("editMenu.deleteLogSiteMenuItem.label"))
  val addNotifier = new MenuItem(i18n("editMenu.addNotifier.label"))
  val parseLogsMenuItem = new MenuItem(i18n("editMenu.parseLogsMenuItem.label"))

  val viewMenu = new Menu(i18n("viewMenu.label"))
  val viewLogSites = new MenuItem(i18n("viewMenu.viewLogSites.label"))
  val viewNotifiers = new MenuItem(i18n("viewMenu.viewNotifiers.label"))
  val viewParsingTasks = new MenuItem(i18n("viewMenu.viewParsingTasks.label"))

  fileMenu.items = List(exitMenuItem)
  editMenu.items = List(addLogSiteMenuItem, editLogSiteMenuItem, deleteLogSiteMenuItem, new SeparatorMenuItem(),
    addNotifier, parseLogsMenuItem)
  viewMenu.items = List(viewLogSites, viewNotifiers, viewParsingTasks)
  menus = List(fileMenu, editMenu, viewMenu)
}
