package ttldtor.ui.parsingtask

import java.util.Date

import ttldtor.entities.{LogSite, ParsingTask}

import scalafx.beans.property.ObjectProperty

/**
  * @author ttldtor
  * @since 21.02.2018
  */
class ParsingTaskModel(parsingTask: ParsingTask) {
  val repr: ObjectProperty[ParsingTask] = ObjectProperty(this, "repr", parsingTask)
  val id: ObjectProperty[Long] = ObjectProperty(this, "id", parsingTask.id)
  val logSite: ObjectProperty[LogSite] = ObjectProperty(this, "logSite", parsingTask.logSite)
  val fromDate: ObjectProperty[Date] = ObjectProperty(this, "fromDate", parsingTask.fromDate)
  val toDate: ObjectProperty[Date] = ObjectProperty(this, "toDate", parsingTask.toDate)

  def set(parsingTask: ParsingTask): Unit = {
    repr.value = parsingTask
    id.value = parsingTask.id
    logSite.value = parsingTask.logSite
    fromDate.value = parsingTask.fromDate
    toDate.value = parsingTask.toDate
  }
}
