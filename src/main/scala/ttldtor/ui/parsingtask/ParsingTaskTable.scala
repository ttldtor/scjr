package ttldtor.ui.parsingtask

import java.text.SimpleDateFormat

import com.osinka.i18n.{Lang, Messages => i18n}
import ttldtor.Config

import scalafx.beans.property.StringProperty
import scalafx.scene.control.{TableColumn, TableView}
import scalafx.scene.layout.Priority

/**
  * @author ttldtor
  * @since 06.03.2018
  */
object ParsingTaskTable {
  val DateFormat = new SimpleDateFormat("dd.MM.yyyy")
}

class ParsingTaskTable extends TableView[ParsingTaskModel] {
  implicit val userLang: Lang = Lang(Config.locale.getLanguage)

  private val idColumn = new TableColumn[ParsingTaskModel, Long](i18n("parsingTaskTable.idColumn.headerText")) {
    visible = false
    cellValueFactory = {
      _.value.id
    }
  }

  private val logSiteColumn = new TableColumn[ParsingTaskModel, String](i18n("parsingTaskTable.logSiteColumn.headerText")) {
    cellValueFactory = { f =>
      val ls = f.value.logSite()

      StringProperty(s"[${ls.name} : ${ls.url}]")
    }
  }

  private val fromDateColumn = new TableColumn[ParsingTaskModel, String](i18n("parsingTaskTable.fromDateColumn.headerText")) {
    cellValueFactory = { f =>
      StringProperty(ParsingTaskTable.DateFormat.format(f.value.fromDate()))
    }
  }

  private val toDateColumn = new TableColumn[ParsingTaskModel, String](i18n("parsingTaskTable.toDateColumn.headerText")) {
    cellValueFactory = { f =>
      StringProperty(ParsingTaskTable.DateFormat.format(f.value.toDate()))
    }
  }

  columns ++= List(idColumn, logSiteColumn, fromDateColumn, toDateColumn)
  vgrow = Priority.Always
}
