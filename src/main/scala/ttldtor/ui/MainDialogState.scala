package ttldtor.ui

/**
  * @author ttldtor
  * @since 06.03.2018
  */
object MainDialogState extends Enumeration {
  val NONE, LOG_SITES, PARSING_TASKS = Value
}
