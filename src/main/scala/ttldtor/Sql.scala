package ttldtor

import java.sql.Connection

import org.h2.jdbcx.{JdbcConnectionPool, JdbcDataSource}

/**
  * Simple connection pool
  *
  * @author ttldtor
  * @since 22.11.2017
  */
object Sql {
  lazy val dataSource: JdbcDataSource = getDataSource

  lazy val pool: JdbcConnectionPool = JdbcConnectionPool.create(dataSource)

  def getDataSource: JdbcDataSource = {
    val ds = new JdbcDataSource
    ds.setURL(Config.databaseUrl)
    ds.setUser(Config.user)
    ds.setPassword(Config.password)
    ds
  }

  def getPool(ds: JdbcDataSource): JdbcConnectionPool = {
    JdbcConnectionPool.create(ds)
  }

  def getConnection: Connection = pool.getConnection

  def getConnection(p: JdbcConnectionPool): Connection = p.getConnection
}
