package ttldtor.providers

import java.util.{Calendar, Date}

import org.jsoup.Jsoup
import org.jsoup.nodes.Element
import org.slf4j.{Logger, LoggerFactory}
import ttldtor.Config

import scala.collection.JavaConverters.collectionAsScalaIterableConverter
import scala.util.matching.Regex
import scala.util.{Failure, Success, Try}

/**
  * @author ttldtor
  * @since 09.02.2018
  */
object ChatLogPeriodProvider {

  import ttldtor.utils.DateUtils._

  private val log: Logger = LoggerFactory.getLogger(ChatLogPeriodProvider.getClass)
  private val yearMonthRx: Regex = """(\d+)/""".r

  def apply(url: String): Option[(Date, Date)] =
    Try(impl(Jsoup.connect(url).timeout(Config.connectionTimeout).validateTLSCertificates(false).get.html)) match {
      case Failure(e) =>
        log.error("Error!", e)
        None

      case Success(r) => r
    }

  def impl(content: String): Option[(Date, Date)] = Try {
    val doc = Jsoup.parse(content)
    val anchorElements = doc.getElementsByTag("a")

    if (anchorElements.isEmpty) {
      None
    } else {
      val years = anchorElements.asScala.flatMap(getURl).flatMap {
        case yearMonthRx(year) => Some(year.toInt)
        case _ => None
      }.toList.sorted

      years match {
        case Nil => None
        case l =>
          val calendar = Calendar.getInstance()

          calendar.set(l.head, 0, 1)
          val firstDate = calendar.getTime.withoutTime

          calendar.set(l.last, 11, 31)
          val lastDate = calendar.getTime.withoutTime

          if (lastDate.getTime > System.currentTimeMillis()) {
            Some((firstDate, currentDateWithoutTime))
          } else {
            Some((firstDate, lastDate))
          }
      }
    }
  } match {
    case Failure(e) =>
      log.error("Error!", e)
      None

    case Success(r) => r
  }

  private def getURl(element: Element): Option[String] = element.attr("href") match {
    case "" => None
    case url => Some(url)
  }
}