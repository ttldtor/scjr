package ttldtor.dao

import java.sql.{Connection, Statement}
import java.util.Date

import org.h2.jdbcx.JdbcConnectionPool
import org.slf4j.{Logger, LoggerFactory}
import ttldtor.entities.LogSite
import ttldtor.{Sql, TryWith}

import scala.util.{Failure, Success}

/**
  * @author ttldtor
  * @since 23.11.2017
  */
object LogSiteDao {
  val log: Logger = LoggerFactory.getLogger(LogSiteDao.getClass)

  val CREATE_SQL = "INSERT INTO log_site (name, conference, url, last_parsed_timestamp) VALUES (?, ?, ?, ?)"
  val SAVE_SQL = "UPDATE log_site SET name = ?, conference = ?, url = ?, last_parsed_timestamp = ? WHERE id = ?"
  val DELETE_SQL = "UPDATE log_site SET deleted = TRUE WHERE id = ?"
  val GET_BY_ID_SQL = "SELECT * FROM log_site WHERE id = ? AND (deleted IS NULL OR (deleted IS NOT NULL AND deleted = FALSE))"
  val GET_SQL = "SELECT * FROM log_site WHERE deleted IS NULL OR (deleted IS NOT NULL AND deleted = FALSE)"

  def create(logSite: LogSite)(implicit pool: JdbcConnectionPool = Sql.pool): Option[LogSite] = {
    createImpl(logSite.name, logSite.conference, logSite.url, logSite.lastParsedDate)(pool)
  }

  def createImpl(name: String, conference: String, url: String, lastParsedDate: Date)
                (implicit pool: JdbcConnectionPool = Sql.pool): Option[LogSite] = {
    TryWith(pool.getConnection)(c =>
      TryWith(c.prepareStatement(CREATE_SQL, Statement.RETURN_GENERATED_KEYS)) { st =>
        st.setString(1, name)
        st.setString(2, conference)
        st.setString(3, url)
        st.setLong(4, lastParsedDate.getTime)

        if (st.executeUpdate() == 0) {
          None
        }

        TryWith(st.getGeneratedKeys)(keys =>
          if (keys.next()) {
            Some(LogSite(keys.getLong(1), name, conference, url, lastParsedDate))
          } else {
            None
          }
        )
      }
    ).flatten.flatten match {
      case Success(x) => x
      case Failure(e) =>
        log.error("Can't create the LogSite record", e)
        None
    }
  }

  def save(logSite: LogSite)(implicit pool: JdbcConnectionPool = Sql.pool): Boolean = {
    TryWith(pool.getConnection)(c =>
      TryWith(c.prepareStatement(SAVE_SQL)) { st =>
        st.setString(1, logSite.name)
        st.setString(2, logSite.conference)
        st.setString(3, logSite.url)
        st.setLong(4, logSite.lastParsedDate.getTime)
        st.setLong(5, logSite.id)

        st.executeUpdate() == 1
      }
    ).flatten match {
      case Success(x) => x
      case Failure(e) =>
        log.error("Can't save the LogSite record", e)
        false
    }
  }

  def delete(logSite: LogSite)(implicit pool: JdbcConnectionPool = Sql.pool): Boolean = {
    TryWith(pool.getConnection)(c =>
      TryWith(c.prepareStatement(DELETE_SQL)) { st =>
        st.setLong(1, logSite.id)

        val result = st.executeUpdate() == 1
        ParsingTaskDao.deleteByLogSiteId(logSite.id)(pool)
        result
      }
    ).flatten match {
      case Success(x) =>
        x
      case Failure(e) =>
        log.error("Can't delete the LogSite record", e)
        false
    }
  }

  def getById(id: Long)(implicit pool: JdbcConnectionPool = Sql.pool): Option[LogSite] = {
    TryWith(pool.getConnection)(c =>
      TryWith(c.prepareStatement(GET_BY_ID_SQL)) { st =>
        st.setLong(1, id)

        TryWith(st.executeQuery())(rs =>
          new Iterator[LogSite] {
            override def hasNext: Boolean = rs.next()

            override def next() = LogSite(id = rs.getLong(1), name = rs.getString(2), conference = rs.getString(3),
              url = rs.getString(4), lastParsedDate = new Date(rs.getLong(5)))
          }.toList
        )
      }
    ).flatten.flatten match {
      case Success(x) if x.isEmpty => None
      case Success(x) if x.nonEmpty => Some(x.head)
      case Failure(e) =>
        log.error("Can't retrieve the LogSite record", e)
        None
    }
  }

  def get()(implicit pool: JdbcConnectionPool = Sql.pool): List[LogSite] = {
    TryWith(pool.getConnection)(c =>
      TryWith(c.prepareStatement(GET_SQL))(st =>
        TryWith(st.executeQuery())(rs =>
          new Iterator[LogSite] {
            override def hasNext: Boolean = rs.next()

            override def next() = LogSite(id = rs.getLong(1), name = rs.getString(2), conference = rs.getString(3),
              url = rs.getString(4), lastParsedDate = new Date(rs.getLong(5)))
          }.toList
        )
      )
    ).flatten.flatten match {
      case Success(x) => x
      case Failure(e) =>
        log.error("Can't retrieve the LogSite records", e)
        List.empty
    }
  }
}
