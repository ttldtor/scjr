package ttldtor.dao

import java.sql.{Connection, Statement}
import java.util.Date

import org.h2.jdbcx.JdbcConnectionPool
import org.slf4j.{Logger, LoggerFactory}
import ttldtor.{Sql, TryWith}
import ttldtor.entities.{LogSite, ParsingTask}

import scala.util.{Failure, Success}

/**
  * @author ttldtor
  * @since 16.02.2018
  */
object ParsingTaskDao {
  val log: Logger = LoggerFactory.getLogger(ParsingTaskDao.getClass)

  val CREATE_SQL = "INSERT INTO parsing_task (log_site_id, from_timestamp, to_timestamp) VALUES (?, ?, ?)"
  val SAVE_SQL = "UPDATE parsing_task SET from_timestamp = ?, to_timestamp = ? WHERE id = ?"
  val DELETE_SQL = "DELETE parsing_task WHERE id = ?"
  val DELETE_BY_LOG_SITE_SQL = "DELETE parsing_task WHERE log_site_id = ?"
  val GET_BY_ID_SQL = "SELECT * FROM parsing_task WHERE id = ?"
  val GET_SQL = "SELECT * FROM parsing_task"

  def createImpl(logSite: LogSite, fromDate: Date, toDate: Date)
                (implicit pool: JdbcConnectionPool = Sql.pool): Option[ParsingTask] = {
    TryWith(pool.getConnection)(c =>
      TryWith(c.prepareStatement(CREATE_SQL, Statement.RETURN_GENERATED_KEYS)) { st =>
        st.setLong(1, logSite.id)
        st.setLong(2, fromDate.getTime)
        st.setLong(3, toDate.getTime)

        if (st.executeUpdate() == 0) {
          None
        }

        TryWith(st.getGeneratedKeys)(keys =>
          if (keys.next()) {
            Some(ParsingTask(keys.getLong(1), logSite, fromDate, toDate))
          } else {
            None
          }
        )
      }
    ).flatten.flatten match {
      case Success(x) => x
      case Failure(e) =>
        log.error("Can't create the ParsingTask record", e)
        None
    }
  }

  def create(parsingTask: ParsingTask)(implicit pool: JdbcConnectionPool = Sql.pool): Option[ParsingTask] = {
    createImpl(parsingTask.logSite, parsingTask.fromDate, parsingTask.toDate)(pool)
  }

  def save(parsingTask: ParsingTask)(implicit pool: JdbcConnectionPool = Sql.pool): Boolean = {
    TryWith(pool.getConnection)(c =>
      TryWith(c.prepareStatement(SAVE_SQL)) { st =>
        st.setLong(1, parsingTask.fromDate.getTime)
        st.setLong(2, parsingTask.toDate.getTime)
        st.setLong(3, parsingTask.id)

        st.executeUpdate() == 1
      }
    ).flatten match {
      case Success(x) => x
      case Failure(e) =>
        log.error("Can't save the ParsingTask record", e)
        false
    }
  }

  def delete(parsingTask: ParsingTask)(implicit pool: JdbcConnectionPool = Sql.pool): Boolean = {
    TryWith(pool.getConnection)(c =>
      TryWith(c.prepareStatement(DELETE_SQL)) { st =>
        st.setLong(1, parsingTask.id)

        st.executeUpdate() == 1
      }
    ).flatten match {
      case Success(x) => x
      case Failure(e) =>
        log.error("Can't delete the ParsingTask record", e)
        false
    }
  }

  def deleteByLogSiteId(logSiteId: Long)(implicit pool: JdbcConnectionPool = Sql.pool): Boolean = {
    TryWith(pool.getConnection)(c =>
      TryWith(c.prepareStatement(DELETE_BY_LOG_SITE_SQL)) { st =>
        st.setLong(1, logSiteId)

        st.executeUpdate() == 1
      }
    ).flatten match {
      case Success(x) => x
      case Failure(e) =>
        log.error("Can't delete the ParsingTask record", e)
        false
    }
  }

  def getById(id: Long)(implicit pool: JdbcConnectionPool = Sql.pool): Option[ParsingTask] = {
    TryWith(pool.getConnection)(c =>
      TryWith(c.prepareStatement(GET_BY_ID_SQL)) { st =>
        st.setLong(1, id)

        TryWith(st.executeQuery())(rs =>
          new Iterator[ParsingTask] {
            override def hasNext: Boolean = rs.next()

            override def next() = ParsingTask(
              id = rs.getLong(1),
              logSite = LogSiteDao.getById(rs.getLong(2))(pool).get,
              fromDate = new Date(rs.getLong(3)),
              toDate = new Date(rs.getLong(4)))
          }.toList
        )
      }
    ).flatten.flatten match {
      case Success(x) if x.isEmpty => None
      case Success(x) if x.nonEmpty => Some(x.head)
      case Failure(e) =>
        log.error("Can't retrieve the ParsingTask record", e)
        None
    }
  }

  def get()(implicit pool: JdbcConnectionPool = Sql.pool): List[ParsingTask] = {
    TryWith(pool.getConnection)(c =>
      TryWith(c.prepareStatement(GET_SQL))(st =>
        TryWith(st.executeQuery())(rs =>
          new Iterator[ParsingTask] {
            override def hasNext: Boolean = rs.next()

            override def next() = ParsingTask(
              id = rs.getLong(1),
              logSite = LogSiteDao.getById(rs.getLong(2))(pool).get,
              fromDate = new Date(rs.getLong(3)),
              toDate = new Date(rs.getLong(4)))
          }.toList
        )
      )
    ).flatten.flatten match {
      case Success(x) => x
      case Failure(e) =>
        log.error("Can't retrieve the ParsingTask records", e)
        List.empty
    }
  }
}
