package ttldtor

import java.util.Locale
import javafx.application.Application

import org.flywaydb.core.Flyway
import org.h2.tools.Script
import org.slf4j.{Logger, LoggerFactory}
import ttldtor.BackupStrategy._
import ttldtor.ui.MainGui

import scala.util.{Failure, Success, Try}


/**
  * Main application
  *
  * @author ttldtor
  * @since 22.11.2017
  */
object App {
  val log: Logger = LoggerFactory.getLogger("CJR")

  def main(args: Array[String]): Unit = {
    Locale.setDefault(Config.locale)

    if (backupDatabase(MIGRATION_BACKUP)) {
      val flyway = new Flyway()

      flyway.setDataSource(Sql.dataSource)

      Try({
        flyway.migrate()
        Application.launch(new MainGui().getClass, args: _*)
        backupDatabase(SESSION_BACKUP)
      }) match {
        case Failure(e) =>
          log.error("Could not migrate the database. ", e)

          flyway.clean()
          restoreDatabase(MIGRATION_BACKUP)

        case Success(_) =>
          log.info("Exiting...")
      }
    }
  }

  def backupDatabase(strategy: BackupStrategy): Boolean = {
    log.info(s"The backup creating is started. Strategy: ${strategy.name}")

    val backupIsCreated = TryWith(Sql.getConnection)(c =>
      Script.process(c, s"./db_backup/${strategy.path}", "", "COMPRESSION ZIP")
    ) match {
      case Success(_) => true
      case Failure(e) =>
        log.error("Could not backup the database. ", e)
        false
    }

    log.info("The backup creating is finished: {}", if (backupIsCreated) "OK" else "FAIL")

    backupIsCreated
  }

  def restoreDatabase(strategy: BackupStrategy): Boolean = {
    log.info(s"The database restoring is started. Strategy: ${strategy.name}")

    val databaseIsRestored = TryWith(Sql.getConnection)(c =>
      TryWith(c.prepareStatement(s"RUNSCRIPT FROM './db_backup/${strategy.path}' COMPRESSION ZIP"))(_.execute())
    ).flatten match {
      case Success(_) => true
      case Failure(e) =>
        log.error("Could not restore the database. ", e)
        false
    }

    log.info("The database restoring is finished: {}", if (databaseIsRestored) "OK" else "FAIL")
    databaseIsRestored
  }
}
