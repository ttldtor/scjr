package ttldtor.utils

import java.util.{Calendar, Date}

/**
  * @author ttldtor
  * @since 09.02.2018
  */
object DateUtils {

  implicit class DateOps(repr: Date) {
    def withoutTime: Date = {
      val calendar = Calendar.getInstance()

      calendar.setTime(repr)
      calendar.set(Calendar.HOUR_OF_DAY, 0)
      calendar.set(Calendar.MINUTE, 0)
      calendar.set(Calendar.SECOND, 0)
      calendar.set(Calendar.MILLISECOND, 0)

      calendar.getTime
    }
  }

  def currentDateWithoutTime: Date = new Date().withoutTime

  /**
    * @param year  The value used to set the <code>YEAR</code> calendar field.
    * @param month The value used to set the <code>MONTH</code> calendar field.
    *              Month value is 0-based. e.g., 0 for January.
    * @param day   The value used to set the <code>DAY_OF_MONTH</code> calendar field.
    * @return date without time
    */
  def dateWithoutTime(year: Int, month: Int, day: Int): Date = {
    val calendar = Calendar.getInstance()

    calendar.set(year, month, day, 0, 0, 0)
    calendar.set(Calendar.MILLISECOND, 0)

    calendar.getTime
  }
}
