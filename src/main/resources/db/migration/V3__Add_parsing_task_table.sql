CREATE TABLE parsing_task (
  id IDENTITY PRIMARY KEY NOT NULL,
  log_site_id bigint UNIQUE NOT NULL,
  from_timestamp bigint NOT NULL,
  to_timestamp bigint NOT NULL,
  CONSTRAINT parsing_task_log_site_id_fk FOREIGN KEY (log_site_id) REFERENCES log_site (id) ON DELETE CASCADE ON UPDATE CASCADE
);