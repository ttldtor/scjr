package ttldtor

import org.scalatest.FunSuite
import ttldtor.providers.ChatLogPeriodProvider

/**
  * @author ttldtor
  * @since 09.02.2018
  */
class ChatLogPeriodProviderSuite extends FunSuite {

  import ttldtor.utils.DateUtils._

  test("empty") {
    assert(ChatLogPeriodProvider.impl("").isEmpty)
  }

  test("simple") {
    val html =
      """
        |<html><head><title>Index of /c_plus_plus@conference.jabber.ru/</title></head>
        |<body><h1>Index of /c_plus_plus@conference.jabber.ru/</h1><hr><pre>
        |<a href="../">../</a><a href="2016/">2016/</a><a href="2017/">2017/</a>
        |</pre><hr></body></html>
      """.stripMargin

    val result = ChatLogPeriodProvider.impl(html)

    assert(result.nonEmpty)

    for {
      (first, last) <- result
    } yield {
      assert(first.getTime.equals(dateWithoutTime(2016, 0, 1).getTime))
      assert(last.getTime.equals(dateWithoutTime(2017, 11, 31).getTime))
    }
  }
}