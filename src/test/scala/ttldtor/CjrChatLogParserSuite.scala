package ttldtor

import org.scalatest.FunSuite
import ttldtor.parsers.CjrChatLogParser

/**
  * @author ttldtor
  * @since 30.01.2018
  */
class CjrChatLogParserSuite extends FunSuite {

  import ttldtor.utils.DateUtils._

  val parser = new CjrChatLogParser()

  test("empty") {
    val result = parser.parse(currentDateWithoutTime, """<html><body></body></html>""")

    assert(result.enterEvents.isEmpty)
    assert(result.exitEvents.isEmpty)
    assert(result.messageEvents.isEmpty)
    assert(result.thirdPersonMessageEvents.isEmpty)
    assert(result.allEvents.isEmpty)
  }

  test("simple enter & exit") {
    val result = parser.parse(currentDateWithoutTime,
      """|<html>
         |   <body>
         |       <a name="20:15:42.123" href="#20:15:42.123" class="ts">[20:15:42]</a>
         |       <font class="ml">ckorzhik вышел из конференции</font><br/>
         |       <a name="20:37:16" href="#20:37:16" class="ts">[20:37:16]</a>
         |       <font class="mj">ħ зашёл в конференцию</font>
         |   </body>
         |</html>""".stripMargin)

    assert(result.enterEvents.lengthCompare(1) == 0)
    assert(result.exitEvents.lengthCompare(1) == 0)
    assert(result.messageEvents.isEmpty)
    assert(result.thirdPersonMessageEvents.isEmpty)
    assert(result.allEvents.size == 2)
    assert(result.enterEvents.head.who == "ħ")
    assert(result.exitEvents.head.who == "ckorzhik")
  }

  test("enter & exit + nicks with spaces") {
    val result = parser.parse(currentDateWithoutTime,
      """|<html>
         |   <body>
         |       <a name="20:15:42.123" href="#20:15:42.123" class="ts">[20:15:42]</a>
         |       <font class="ml">uggi 345 вышел(а) из комнаты</font><br/>
         |       <a name="20:37:16" href="#20:37:16" class="ts">[20:37:16]</a>
         |       <font class="mj">ħ 123 зашёл в конференцию</font>
         |   </body>
         |</html>""".stripMargin)

    assert(result.enterEvents.lengthCompare(1) == 0)
    assert(result.exitEvents.lengthCompare(1) == 0)
    assert(result.messageEvents.isEmpty)
    assert(result.thirdPersonMessageEvents.isEmpty)
    assert(result.allEvents.size == 2)
    assert(result.enterEvents.head.who == "ħ 123")
    assert(result.exitEvents.head.who == "uggi 345")
  }

  test("enter & exit + nicks with spaces + wrong timestamps") {
    val result = parser.parse(currentDateWithoutTime,
      """|<html>
         |   <body>
         |       <a name="asdasd" href="#20:15:42.123" class="ts">[20:15:42]</a>
         |       <font class="ml">uggi 345 вышел(а) из комнаты</font><br/>
         |       <a name="123123" href="#20:37:16" class="ts">[20:37:16]</a>
         |       <font class="mj">ħ 123 зашёл в конференцию</font>
         |   </body>
         |</html>""".stripMargin)

    assert(result.enterEvents.isEmpty)
    assert(result.exitEvents.isEmpty)
    assert(result.messageEvents.isEmpty)
    assert(result.thirdPersonMessageEvents.isEmpty)
    assert(result.allEvents.isEmpty)
  }

  test("enter & exit + nicks with spaces + wrong timestamps + message") {
    val result = parser.parse(currentDateWithoutTime,
      """|<html>
         |   <body>
         |       <a name="abcd" href="#20:15:42.123" class="ts">[20:15:42]</a>
         |       <font class="ml">uggi 345 вышел(а) из комнаты</font><br/>
         |       <a name="123213123" href="#20:37:16" class="ts">[20:37:16]</a>
         |       <font class="mj">ħ 123 зашёл в конференцию</font>
         |       <a id="11:14:54.394156" name="11:14:54.394156" href="#11:14:54.394156" class="ts">[11:14:54]</a>
         |       <font class="mn">&lt;ħ&gt;</font> где мантикор?<br>
         |   </body>
         |</html>""".stripMargin)

    assert(result.enterEvents.isEmpty)
    assert(result.exitEvents.isEmpty)
    assert(result.messageEvents.lengthCompare(1) == 0)
    assert(result.messageEvents.head.who == "ħ")
    assert(result.messageEvents.head.message == "где мантикор?")
    assert(result.thirdPersonMessageEvents.isEmpty)
    assert(result.allEvents.size == 1)
  }

  test("multi-line messages + links + 6 digit millis") {
    val result = parser.parse(currentDateWithoutTime,
      """|<html>
         |   <body>
         |       <a id="00:08:29.482382" name="00:08:29.482382" href="#00:08:29.482382" class="ts">[00:08:29]</a>
         |       <font class="mn">&lt;gr_buza@arbeiten&gt;</font> 153-34-168-192:~ eill$ host draw.io<br/>draw.io has address 216.239.38.21<br/>draw.io has address 216.239.34.21<br/>draw.io has address 216.239.36.21<br/>draw.io has address 216.239.32.21<br/>draw.io has IPv6 address 2001:4860:4802:38::15<br/>draw.io has IPv6 address 2001:4860:4802:32::15<br/>draw.io has IPv6 address 2001:4860:4802:34::15<br/>draw.io has IPv6 address 2001:4860:4802:36::15<br/>draw.io mail is handled by 5 alt1.aspmx.l.google.com.<br/>draw.io mail is handled by 10 aspmx2.googlemail.com.<br/>draw.io mail is handled by 5 alt2.aspmx.l.google.com.<br/>draw.io mail is handled by 1 aspmx.l.google.com.<br/>
         |       <a id="00:08:33.384387" name="00:08:33.384387" href="#00:08:33.384387" class="ts">[00:08:33]</a>
         |       <font class="mn">&lt;gr_buza@arbeiten&gt;</font> держи <a href="http://lenta.ru/news/2016/03/25/baldyandex/" rel="nofollow">http://lenta.ru/news/2016/03/25/baldyandex/</a><br/>
         |   </body>
         |</html>""".stripMargin)

    assert(result.enterEvents.isEmpty)
    assert(result.exitEvents.isEmpty)
    assert(result.messageEvents.lengthCompare(2) == 0)
    assert(result.messageEvents.head.who == "gr_buza@arbeiten")
    assert(result.messageEvents.head.message == "153-34-168-192:~ eill$ host draw.io\ndraw.io has address 216.239.38.21\ndraw.io has address 216.239.34.21\ndraw.io has address 216.239.36.21\ndraw.io has address 216.239.32.21\ndraw.io has IPv6 address 2001:4860:4802:38::15\ndraw.io has IPv6 address 2001:4860:4802:32::15\ndraw.io has IPv6 address 2001:4860:4802:34::15\ndraw.io has IPv6 address 2001:4860:4802:36::15\ndraw.io mail is handled by 5 alt1.aspmx.l.google.com.\ndraw.io mail is handled by 10 aspmx2.googlemail.com.\ndraw.io mail is handled by 5 alt2.aspmx.l.google.com.\ndraw.io mail is handled by 1 aspmx.l.google.com.")
    assert(result.messageEvents(1).who == "gr_buza@arbeiten")
    assert(result.messageEvents(1).message == "держи http://lenta.ru/news/2016/03/25/baldyandex/")
    assert(result.thirdPersonMessageEvents.isEmpty)
    assert(result.allEvents.size == 2)
  }

  test("3rd person message") {
    val result = parser.parse(currentDateWithoutTime,
      """|<html>
         |   <body>
         |       <a id="21:17:49.826752" name="21:17:49.826752" href="#21:17:49.826752" class="ts">[21:17:49]</a>
         |       <font class="mne">ermine покорячила говядину - чем свежее говядина, тем существующий код менее рабочий</font><br/>
         |   </body>
         |</html>""".stripMargin)

    assert(result.enterEvents.isEmpty)
    assert(result.exitEvents.isEmpty)
    assert(result.messageEvents.isEmpty)
    assert(result.thirdPersonMessageEvents.lengthCompare(1) == 0)
    assert(result.thirdPersonMessageEvents.head.who == "ermine")
    assert(result.thirdPersonMessageEvents.head.message == "покорячила говядину - чем свежее говядина, тем существующий код менее рабочий")
    assert(result.allEvents.size == 1)
  }

  test("3rd person message + nicks ordering") {
    val result = parser.parse(currentDateWithoutTime,
      """|<html>
         |   <body>
         |       <a name="20:15:42.123" href="#20:15:42.123" class="ts">[20:15:42]</a>
         |       <font class="mj">ħ 123 зашёл в конференцию</font>
         |       <a name="20:37:16" href="#20:37:16" class="ts">[20:37:16]</a>
         |       <font class="mj">ħ зашёл в конференцию</font>
         |       <a id="21:17:49.826752" name="21:17:49.826752" href="#21:17:49.826752" class="ts">[21:17:49]</a>
         |       <font class="mne">ħ 123 покорячил говядину - чем свежее говядина, тем существующий код менее рабочий</font><br/>
         |   </body>
         |</html>""".stripMargin)

    assert(result.enterEvents.lengthCompare(2) == 0)
    assert(result.exitEvents.isEmpty)
    assert(result.messageEvents.isEmpty)
    assert(result.thirdPersonMessageEvents.lengthCompare(1) == 0)
    assert(result.thirdPersonMessageEvents.head.who == "ħ 123")
    assert(result.thirdPersonMessageEvents.head.message == "покорячил говядину - чем свежее говядина, тем существующий код менее рабочий")
    assert(result.allEvents.size == 3)
  }

}
