package ttldtor

import java.util.{Date, Locale}

import org.flywaydb.core.Flyway
import org.h2.jdbcx.JdbcConnectionPool
import org.scalatest.FunSuite
import ttldtor.dao.{LogSiteDao, ParsingTaskDao}
import ttldtor.entities.LogSite

/**
  * @author ttldtor
  * @since 24.11.2017
  */
class DaoSuite extends FunSuite {
  Locale.setDefault(Config.locale)

  def getPool: JdbcConnectionPool = {
    val ds = Sql.getDataSource
    val flyway = new Flyway()

    flyway.setDataSource(ds)
    flyway.clean()
    flyway.migrate()

    Sql.getPool(ds)
  }

  test("LogSiteDao create") {
    implicit val pool: JdbcConnectionPool = getPool

    val logSite = LogSiteDao.createImpl("", "", "", new Date)
    assert(logSite.isDefined)
    assert(LogSiteDao.create(LogSite("", "", "", new Date)).isEmpty)
    LogSiteDao.create(LogSite("1", "", "", new Date))
    LogSiteDao.create(LogSite("2", "", "", new Date))

    logSite match {
      case Some(l) =>
        assert(LogSiteDao.getById(l.id).isDefined)
      case _ =>
    }

    assert(LogSiteDao.get().lengthCompare(3) == 0)
  }

  test("LogSiteDao delete") {
    implicit val pool: JdbcConnectionPool = getPool

    assert(!LogSiteDao.delete(LogSite("1", "", "", new Date)))

    assert(LogSiteDao.get().lengthCompare(0) == 0)
    val logSite = LogSiteDao.createImpl("", "", "", new Date)
    assert(LogSiteDao.get().lengthCompare(1) == 0)
    assert(LogSiteDao.delete(logSite.get))
    assert(LogSiteDao.get().lengthCompare(0) == 0)
  }

  test("LogSiteDao save") {
    implicit val pool: JdbcConnectionPool = getPool

    LogSiteDao.createImpl("000", "", "", new Date)
    LogSiteDao.createImpl("111", "", "", new Date) match {
      case Some(l) =>
        assert(LogSiteDao.save(LogSite(l.id, "222", l.conference, l.url, l.lastParsedDate)))
        LogSiteDao.getById(l.id) match {
          case Some(l2) =>
            assert(l2.name == "222")
            assert(!LogSiteDao.save(LogSite(l.id, "000", l.conference, l.url, l.lastParsedDate)))
          case _ =>
        }
      case _ =>
    }
  }

  test("ParsingTaskDao create") {
    implicit val pool: JdbcConnectionPool = getPool
    val parsingTask = ParsingTaskDao.createImpl(LogSite(0, "", "", "", new Date), new Date, new Date)

    assert(parsingTask.isEmpty)

    LogSiteDao.createImpl("111", "", "", new Date) match {
      case Some(l) =>
        val parsingTask2 = ParsingTaskDao.createImpl(l, new Date, new Date)

        assert(parsingTask2.nonEmpty)
      case _ =>
    }
  }

  test("ParsingTaskDao delete") {
    implicit val pool: JdbcConnectionPool = getPool

    LogSiteDao.createImpl("111", "", "", new Date) match {
      case Some(l) =>
        val parsingTask2 = ParsingTaskDao.createImpl(l, new Date, new Date)

        assert(parsingTask2.nonEmpty)

        if (LogSiteDao.delete(l)) {
          assert(ParsingTaskDao.getById(parsingTask2.get.id).isEmpty)
        }
      case _ =>
    }
  }
}
