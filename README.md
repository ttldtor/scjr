# The `conference.jabber.ru` logs parser + search engine + mentions detector. Scala edition

[![build status](https://gitlab.com/ttldtor/scjr/badges/master/build.svg)](https://gitlab.com/ttldtor/scjr/commits/master)
[![coverage report](https://gitlab.com/ttldtor/scjr/badges/master/coverage.svg)](https://gitlab.com/ttldtor/scjr/commits/master)

<!-- 2022.12.11 Just in case GitLab doesn't delete the project due to inactivity. -->
